package edu.uci.ics.skillpredictor.dataconverter.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import edu.uci.ics.skillpredictor.dataconverter.dataAccess.ExportedDataDAO;
import edu.uci.ics.skillpredictor.dataconverter.model.RawSkillData;
import edu.uci.ics.skillpredictor.dataconverter.model.SkillDataType;
import edu.uci.ics.skillpredictor.dataconverter.profile.ContributionTokenizer;

public class DBExporter {

	private ContributionTokenizer tokenizer;

	public DBExporter() {
		tokenizer = new ContributionTokenizer();
	}

	public void init() throws IOException {
		tokenizer.initialize();
	}

	public void exportData(String author, Map<SkillDataType, List<RawSkillData>> userData) throws IOException {
		if (!userData.isEmpty()) {

			exportToFile(author, userData);
		}
	}

	private void exportToFile(String author, Map<SkillDataType, List<RawSkillData>> userData) throws IOException {
		BufferedWriter writer = Files.newBufferedWriter(getExportPath(author), Charset.defaultCharset(), StandardOpenOption.CREATE_NEW);
		for (Entry<SkillDataType, List<RawSkillData>> userContribs : userData.entrySet()) {

			for (String term : tokenize(userContribs.getValue())) {
				writer.write(term);
				writer.write('\n');
			}
		}
		writer.close();
	}

	private Path getExportPath(String author) {
		return new File("output/" + author + ".dat").toPath();
	}

	private void exportToMongoDB(String author, Map<SkillDataType, List<RawSkillData>> userData) throws UnknownHostException, IOException {
		ExportedDataDAO exportedDataDAO = new ExportedDataDAO();

		for (Entry<SkillDataType, List<RawSkillData>> userContribs : userData.entrySet()) {
			exportedDataDAO.insertData(author, userContribs.getKey().getName(), tokenize(userContribs.getValue()));
		}
	}

	private List<String> tokenize(List<RawSkillData> value) throws IOException {
		return tokenizer.tokenize(value);
	}

	public void close() {
		tokenizer.close();
	}
}