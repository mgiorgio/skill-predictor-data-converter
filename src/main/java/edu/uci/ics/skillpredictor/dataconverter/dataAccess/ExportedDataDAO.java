package edu.uci.ics.skillpredictor.dataconverter.dataAccess;

import java.net.UnknownHostException;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

public class ExportedDataDAO {

	public static final String tableName = "contributions";
	public static final String databaseName = "msr14tokenized";

	public ExportedDataDAO() {
	}

	/**
	 * @param helper
	 * @return DBCollection table
	 * @throws UnknownHostException
	 */
	private DBCollection getTable() throws UnknownHostException {
		MongoClient DbClient = MongoDBHelper.getConnection(new ServerAddress("localhost", 27017));
		DB database = MongoDBHelper.getDatabase(databaseName, DbClient);
		DBCollection table = MongoDBHelper.getTable(tableName, database);
		return table;
	}

	public void insertData(String author, String type, List<String> terms) throws UnknownHostException {
		BasicDBObject document = new BasicDBObject();
		document.put("author", author);
		document.put("type", type);
		document.put("contributions", terms);
		MongoDBHelper.export(getTable(), document);
	}
}
