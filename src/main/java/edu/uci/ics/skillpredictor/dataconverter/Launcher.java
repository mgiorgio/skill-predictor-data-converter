package edu.uci.ics.skillpredictor.dataconverter;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.skillpredictor.dataconverter.controller.MongoDBDataRetriever;
import edu.uci.ics.skillpredictor.dataconverter.dataAccess.DataAccessException;
import edu.uci.ics.skillpredictor.dataconverter.export.DBExporter;

/**
 * @author matias
 * 
 */
public class Launcher {

	private static final Logger console = LoggerFactory.getLogger("console");

	public Launcher() {
	}

	public static void main(String[] args) {
		console.info("Launching Skill Predictor Data Converter...");

		try {
			DBExporter dbExporter = new DBExporter();
			dbExporter.init();
			MongoDBDataRetriever dataRetriever = new MongoDBDataRetriever(dbExporter);
			normalizeData(args, dataRetriever);
			dbExporter.close();
		} catch (DataAccessException | IOException e) {
			console.error("Skill Predictor Data Converter had a problem: {}", e.getMessage());
		}

		console.info("Skill Predictor Data Converter shut down.");
	}

	private static void normalizeData(String[] args, MongoDBDataRetriever dataRetriever) throws DataAccessException, IOException {
		List<String> devs = readDevsFile();
		if (isFixedNumberOfUsers(devs)) {
			console.info("Profiling authors {}", StringUtils.join(devs.toArray(new String[devs.size()])));
			dataRetriever.retrieve(devs, new LinkedList<String>());
		} else {
			dataRetriever.retrieve();
		}
	}

	private static List<String> readDevsFile() throws IOException {
		return Files.readAllLines(new File("cfg/devs.txt").toPath(), Charset.defaultCharset());
	}

	private static boolean isFixedNumberOfUsers(List<String> devs) {
		return !devs.isEmpty();
	}
}