package edu.uci.ics.skillpredictor.dataconverter.config;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import edu.uci.ics.skillpredictor.dataconverter.lucene.Utils;

/**
 * @author matias
 * 
 */
public class ConfigurationUtils {

	private static PropertiesConfiguration config;

	private static Set<String> repoURLs;

	static {
		try {
			repoURLs = new HashSet<>(Files.readAllLines(new File("cfg/repos.txt").toPath(), Charset.defaultCharset()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static PropertiesConfiguration getConfig() {
		if (config == null) {

			try {
				config = new PropertiesConfiguration(ConfigurationUtils.getConfigurationPath());

			} catch (ConfigurationException e) {
				throw new RuntimeException(e.getMessage());
			}
		}
		return config;
	}

	public static String getConfigurationPath() {
		return Utils.CONFIG + File.separator + Utils.CONFIG_FILENAME;
	}

	public static Set<String> repoURLs() {
		return repoURLs;
	}
}
