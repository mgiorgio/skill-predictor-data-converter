/**
 * 
 */
package edu.uci.ics.skillpredictor.dataconverter.profile;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.miscellaneous.LengthFilter;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterFilter;
import org.apache.lucene.analysis.pattern.PatternReplaceFilter;
import org.apache.lucene.analysis.util.CharArraySet;

import edu.uci.ics.skillpredictor.dataconverter.lucene.Utils;

/**
 * @author Matias
 * 
 */
public class GithubAnalyzer extends Analyzer {

	private List<String> stopWords;

	public GithubAnalyzer() throws IOException {
		stopWords = createStopWords();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.lucene.analysis.Analyzer#createComponents(java.lang.String,
	 * java.io.Reader)
	 */
	@Override
	protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
		WhitespaceTokenizer source = new WhitespaceTokenizer(Utils.LUCENE_VERSION, reader);

		TokenStream result = null;

		int configurationFlags = WordDelimiterFilter.GENERATE_WORD_PARTS | WordDelimiterFilter.SPLIT_ON_CASE_CHANGE;

		result = new WordDelimiterFilter(source, configurationFlags, null);

		result = new ASCIIFoldingFilter(result);

		result = new LowerCaseFilter(Utils.LUCENE_VERSION, result);

		// Non alphanumeric words.
		result = new PatternReplaceFilter(result, Pattern.compile("^.*[^a-zA-Z0-9].*$"), "", false);

		// Hexa
		result = new PatternReplaceFilter(result, Pattern.compile("^[a-fA-F0-9]*[0-9]+[a-fA-F0-9]*$"), "", false);

		// Hexa 2
		result = new PatternReplaceFilter(result, Pattern.compile("^0x[0-9a-fA-F]+$"), "", false);

		result = new LengthFilter(Utils.LUCENE_VERSION, result, 2, Integer.MAX_VALUE);

//		result = new PorterStemFilter(result);

		result = new StopFilter(Utils.LUCENE_VERSION, result, new CharArraySet(Utils.LUCENE_VERSION, stopWords, false));

		TokenStreamComponents components = new TokenStreamComponents(source, result);

		return components;
	}

	private List<String> createStopWords() throws IOException {
		List<String> stopWords = Files.readAllLines(new File("cfg/stop_words.txt").toPath(), Charset.forName("ASCII"));
		for (char c : "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray()) {
			stopWords.add(Character.toString(c));
		}
		return stopWords;
	}
}