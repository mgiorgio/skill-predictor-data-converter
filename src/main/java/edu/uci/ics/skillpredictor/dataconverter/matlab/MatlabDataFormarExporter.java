package edu.uci.ics.skillpredictor.dataconverter.matlab;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

public class MatlabDataFormarExporter extends MatlabCompressDataFormatExporter {

	public static final String DS_BAG_OF_WORDS_FILEPATH = MatlabDataConverter.MATLAB_OUTPUT_FOLDER + "/ds_bagofwords.txt";
	public static final String WS_BAG_OF_WORDS_FILEPATH = MatlabDataConverter.MATLAB_OUTPUT_FOLDER + "/ws_bagofwords.txt";

	private BufferedWriter dsBagOfWords;

	private BufferedWriter wsBagOfWords;

	public MatlabDataFormarExporter() {
		super();
	}

	@Override
	public void init() throws IOException {
		dsBagOfWords = new BufferedWriter(new FileWriter(new File(DS_BAG_OF_WORDS_FILEPATH)));
		wsBagOfWords = new BufferedWriter(new FileWriter(new File(WS_BAG_OF_WORDS_FILEPATH)));
	}

	@Override
	public void close() throws IOException {
		dsBagOfWords.close();
		wsBagOfWords.close();
	}

	public void export(Path path, String author) throws IOException {
		Integer authorIndex = this.getAuthorIndex(author);
		BufferedReader bufferedReader = Files.newBufferedReader(path, Charset.defaultCharset());

		String line;
		while ((line = bufferedReader.readLine()) != null) {
			dsBagOfWords.write(authorIndex);
			dsBagOfWords.write('\n');
			wsBagOfWords.write(getIndex(line).toString());
			wsBagOfWords.write('\n');
		}

		bufferedReader.close();

		// List<String> words = Files.readAllLines(path,
		// Charset.defaultCharset());

		// String ds = StringUtils.repeat(String.valueOf(authorIndex), "\n",
		// words.size());
		// ds += "\n";

		// Files.write(new File(DS_BAG_OF_WORDS_FILEPATH).toPath(),
		// ds.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
		// dsBagOfWords.write(ds);

		// String[] wordIndices = new String[words.size()];
		// int i = 0;
		// for (String word : words) {
		// // wordIndices[i++] = String.valueOf(getIndex(word));
		// dsBagOfWords.write(authorIndex);
		// dsBagOfWords.write('\n');
		// wsBagOfWords.write(getIndex(word).toString());
		// wsBagOfWords.write('\n');
		// }
		// String ws = StringUtils.join(wordIndices, "\n");
		// ws += "\n";
		// Files.write(new File(WS_BAG_OF_WORDS_FILEPATH).toPath(),
		// ws.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);

	}
}
