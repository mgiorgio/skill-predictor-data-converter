package edu.uci.ics.skillpredictor.dataconverter.dataAccess;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;

/**
 * @author shriti Utility to operate over the database
 */
public class MongoDBHelper {

	private static Map<ServerAddress, MongoClient> cachedConnections = new HashMap<ServerAddress, MongoClient>();

	private static Map<String, DB> cachedDBs = new HashMap<>();

	public static MongoClient getConnection(ServerAddress address) throws UnknownHostException {
		MongoClient mongoClient = cachedConnections.get(address);

		if (mongoClient == null) {
			mongoClient = new MongoClient(address);
			cachedConnections.put(address, mongoClient);
		}
		return mongoClient;
	}

	public static DB getDatabase(String databaseName, MongoClient mongo) {
		DB db = cachedDBs.get(databaseName);
		if (db == null) {
			db = mongo.getDB(databaseName);
			cachedDBs.put(databaseName, db);
		}
		return db;
	}

	public static Set<String> getAllTables(DB db) {
		return (db.getCollectionNames());
	}

	public static DBCollection getTable(String tableName, DB db) {
		return (db.getCollection(tableName));
	}

	public static DBCursor findData(BasicDBObject searchQuery, DBCollection table, BasicDBObject fields) {
		if (fields == null)
			return (table.find(searchQuery));
		else
			return (table.find(searchQuery, fields));
	}

	public static void export(DBCollection table, DBObject objectToBeInserted) {
		table.insert(objectToBeInserted);
	}

	public static void export(DBCollection table, List<DBObject> objectsToBeInserted) {
		table.insert(objectsToBeInserted);
	}
}
