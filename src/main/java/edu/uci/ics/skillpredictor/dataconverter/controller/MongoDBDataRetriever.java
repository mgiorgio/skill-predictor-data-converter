package edu.uci.ics.skillpredictor.dataconverter.controller;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.skillpredictor.dataconverter.dataAccess.AuthorAndUserDAO;
import edu.uci.ics.skillpredictor.dataconverter.dataAccess.DataAccessException;
import edu.uci.ics.skillpredictor.dataconverter.dataAccess.DataAggregator;
import edu.uci.ics.skillpredictor.dataconverter.export.DBExporter;
import edu.uci.ics.skillpredictor.dataconverter.model.RawSkillData;
import edu.uci.ics.skillpredictor.dataconverter.model.SkillDataType;

/**
 * @author matias
 * 
 */
public class MongoDBDataRetriever {

	private static final Logger console = LoggerFactory.getLogger("console");

	private DBExporter dbExporter;

	public MongoDBDataRetriever(DBExporter dbExporter) {
		this.dbExporter = dbExporter;
	}

	public void retrieve(String author) throws DataAccessException, IOException {
		this.retrieve(Arrays.asList(author), new ArrayList<String>());
	}

	public void retrieveExcluding(List<String> exclude) throws DataAccessException, IOException {
		this.retrieve(this.retrieveLogins(), exclude);
	}

	public void retrieve() throws DataAccessException, IOException {
		this.retrieveExcluding(new ArrayList<String>());
	}

	public void retrieve(List<String> logins, List<String> exclude) throws DataAccessException, IOException {
		@SuppressWarnings("unchecked")
		List<String> include = ListUtils.removeAll(logins, exclude);

		int totalLoginsToProfile = include.size();
		int currentProfiledUser = 0;
		console.info("About to profile {} users...", totalLoginsToProfile);
		for (String login : include) {
			console.info("Profiling user {}...", login);

			currentProfiledUser++;

			// Collect the RawSkillData objects associated to each login.
			console.info("Retrieving Raw Skill Data from Github for {} ({} / {})...", login, currentProfiledUser, totalLoginsToProfile);
			Map<SkillDataType, List<RawSkillData>> rawDataForLogin = this.retrieveSkillDataForLogin(login);

			// Index the obtained objects.
			console.info("Creating User Ranking for {}...", login);
			dbExporter.exportData(login, rawDataForLogin);
		}
	}

	/**
	 * Retrieves all contributions for a given login. A contribution can be any
	 * element from the {@link SkillDataType} enumeration.
	 * 
	 * @param login
	 * @return The list of contributions.
	 * @throws UnknownHostException
	 */
	private Map<SkillDataType, List<RawSkillData>> retrieveSkillDataForLogin(String login) throws UnknownHostException {
		DataAggregator dataAgg = new DataAggregator();
		return dataAgg.getAuthorData(login);
	}

	/**
	 * Obtains a {@link List} of logins from MongoDB.
	 * 
	 * @return A {@link List} of Strings representing the logins.
	 * @throws UnknownHostException
	 */
	private List<String> retrieveLogins() throws UnknownHostException {
		AuthorAndUserDAO dao = new AuthorAndUserDAO();
		return dao.getLoginList();
	}

}
