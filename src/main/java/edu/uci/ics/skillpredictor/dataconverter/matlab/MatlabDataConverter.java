package edu.uci.ics.skillpredictor.dataconverter.matlab;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.MalformedInputException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class MatlabDataConverter {

	public static final String OUTPUT_FOLDER = "output";

	public static final String MATLAB_OUTPUT_FOLDER = OUTPUT_FOLDER + "/matlab";

<<<<<<< HEAD
=======
	public static final String DS_BAG_OF_WORDS_FILEPATH = MATLAB_OUTPUT_FOLDER + "/ds_bagofwords.txt";
	public static final String WS_BAG_OF_WORDS_FILEPATH = MATLAB_OUTPUT_FOLDER + "/ws_bagofwords.txt";

>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660
	public static final String VOCABULARY_FILEPATH = MATLAB_OUTPUT_FOLDER + "/wo_vocabulary.txt";
	public static final String WO_VOCABULARY_FILEPATH = MATLAB_OUTPUT_FOLDER + "/vocabulary.txt";

	public static final String AUTHORS_TABLE_FILEPATH = MATLAB_OUTPUT_FOLDER + "/authorstable.txt";
	public static final String AUTHORS_LIST_FILEPATH = MATLAB_OUTPUT_FOLDER + "/authorslist.txt";

	public static void main(String[] args) throws IOException {
<<<<<<< HEAD
		Collection<File> files = FileUtils.listFiles(new File(OUTPUT_FOLDER), TrueFileFilter.INSTANCE, FalseFileFilter.INSTANCE);
		// DirectoryStream<Path> directoryStream = Files.newDirectoryStream(new
		// File(OUTPUT_FOLDER).toPath());

		long SIZE_LIMIT = 750000;
=======
		DirectoryStream<Path> directoryStream;
		if (args.length > 0) {
			directoryStream = Files.newDirectoryStream(new File(OUTPUT_FOLDER).toPath(), args[0]);
		} else {
			directoryStream = Files.newDirectoryStream(new File(OUTPUT_FOLDER).toPath());
		}

		MatlabDataFormatExporter exporter = new MatlabDataFormatExporter();
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660

		if (args.length > 0) {
			SIZE_LIMIT = Long.parseLong(args[0]);
		}

		MatlabCompressDataFormatExporter exporter = new MatlabCompressDataFormatExporter();
		// MatlabCompressDataFormatExporter exporter = new
		// MatlabDataFormarExporter();

		exporter.init();

		int totalDevs = files.size();
		int includedDevs = 0;
		// for (Path path : directoryStream) {
		System.out.println("Total: " + totalDevs);
		for (File file : files) {
			// File file = path.toFile();
			if (file.isFile() && !"invalid-email-address.dat".equals(file.getName())) {
				if (file.length() > SIZE_LIMIT) {
					includedDevs++;
					// String name = path.getFileName().toFile().getName();
					String name = file.getName();
					name = name.substring(0, name.length() - 4); // Remove
																	// ".dat"
					// extension.
					System.out.println("Exporting " + name);
					try {
						exporter.export(file.toPath(), name);
						// exporter.export(path, name);
					} catch (MalformedInputException e) {
						System.err.println(name);
						e.printStackTrace();
					}
				}
			}
		}

		exportVocabulary(exporter);
		exportAuthorsTable(exporter);

		exporter.close();
<<<<<<< HEAD

		System.out.println("Included: " + includedDevs);
=======
		System.out.println("Finished.");
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660
	}

	private static void exportAuthorsTable(MatlabCompressDataFormatExporter exporter) throws IOException {
		Map<String, Integer> table = exporter.getAuthorsTable();

		Files.write(new File(AUTHORS_TABLE_FILEPATH).toPath(), formatPairs(table, "\t"), Charset.defaultCharset(), StandardOpenOption.CREATE);
		Files.write(new File(AUTHORS_LIST_FILEPATH).toPath(), table.keySet(), Charset.defaultCharset(), StandardOpenOption.CREATE);
	}

<<<<<<< HEAD
	private static void exportVocabulary(MatlabCompressDataFormatExporter exporter) throws IOException {
=======
	private static void exportVocabulary(MatlabDataFormatExporter exporter) throws IOException {
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660
		Files.write(new File(WO_VOCABULARY_FILEPATH).toPath(), exporter.getVocabulary().keySet(), Charset.defaultCharset(), StandardOpenOption.CREATE);
		Files.write(new File(VOCABULARY_FILEPATH).toPath(), formatPairs(exporter.getVocabulary(), "\t"), Charset.defaultCharset(), StandardOpenOption.CREATE);
	}

	private static List<String> formatPairs(Map<?, ?> map, String separator) {
		List<String> exportTableString = new ArrayList<>(map.size());

		for (Entry<?, ?> pair : map.entrySet()) {
			exportTableString.add(pair.getKey() + separator + pair.getValue());
		}
		return exportTableString;
	}
}
