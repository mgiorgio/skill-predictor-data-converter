package edu.uci.ics.skillpredictor.dataconverter.profile;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.skillpredictor.dataconverter.lucene.Utils;
import edu.uci.ics.skillpredictor.dataconverter.model.RawSkillData;

/**
 * @author Matias
 * 
 */
public class ContributionTokenizer {

	private static final Logger log = LoggerFactory.getLogger(ContributionTokenizer.class);

	private GithubAnalyzer analyzer;

	public ContributionTokenizer() {
	}

	public List<String> tokenize(List<RawSkillData> rawSkillDataObjects) throws IOException {
		List<String> terms = new LinkedList<>();
		for (RawSkillData rawSkillData : rawSkillDataObjects) {

			final String input = rawSkillData.getContents();

			if (input != null) {

				TokenStream ts;
				ts = analyzer.tokenStream(Utils.Globals.CONTENTS_FIELD, input);

				CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);

				try {
					ts.reset(); // Resets this stream to the beginning.
								// (Required)
					while (ts.incrementToken()) {
						terms.add(termAtt.toString());
					}
					ts.end(); // Perform end-of-stream operations, e.g. set
								// the
								// final
								// offset.
				} finally {
					ts.close(); // Release resources associated with this
								// stream.
				}
			}
		}

		return terms;
	}

	public void initialize() throws IOException {
		analyzer = new GithubAnalyzer();
	}

	public void close() {
		analyzer.close();
	}
}
