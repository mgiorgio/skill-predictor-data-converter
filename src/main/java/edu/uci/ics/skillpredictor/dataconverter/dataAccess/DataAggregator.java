package edu.uci.ics.skillpredictor.dataconverter.dataAccess;

import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;

import edu.uci.ics.skillpredictor.dataconverter.model.RawSkillData;
import edu.uci.ics.skillpredictor.dataconverter.model.SkillDataType;
import edu.uci.ics.skillpredictor.dataconverter.model.db.Comments;
import edu.uci.ics.skillpredictor.dataconverter.model.db.Commit;
import edu.uci.ics.skillpredictor.dataconverter.model.db.PatchedFile;

/**
 * @author shriti Class to bring together all data access objects to produce
 *         basic unit of information for Lucene as <RawSkillData>.
 * 
 */
public class DataAggregator {

	private static final Logger log = LoggerFactory.getLogger(DataAggregator.class);

	private Calendar cachedCalendar = GregorianCalendar.getInstance();

	/**
	 * @param login
	 * @return ArrayList<RawSkillData>
	 * @throws UnknownHostException
	 */
	public Map<SkillDataType, List<RawSkillData>> getAuthorData(String login) throws UnknownHostException {
		log.info("About to query DB for [{}]...", login);
		Map<SkillDataType, List<RawSkillData>> authorData = new HashMap<>();

		ContentDAO contentDAO = new ContentDAO();

		// get commit data
		log.debug("Querying commits for [{}]...", login);
		List<Commit> authorCommits = (List<Commit>) contentDAO.getCommits(login);
		if (!authorCommits.isEmpty()) {
			log.debug("About to process [{}] commits for [{}]...", authorCommits.size(), login);
			Iterator<Commit> it = authorCommits.iterator();
			List<RawSkillData> commitMessages = new LinkedList<>();
			List<RawSkillData> commitPatches = new LinkedList<>();
			while (it.hasNext()) {
				Commit commit = it.next();

				long commitTime = parseTime(commit.getTime());

				// Process commit message.
				RawSkillData commitMessageData = new RawSkillData();
				commitMessageData.setAuthor(login);
				commitMessageData.setType(SkillDataType.COMMIT_MESSAGE);
				commitMessageData.setTimestamp(commitTime);
				commitMessageData.setContents(commit.getCommit_message());

				commitMessages.add(commitMessageData);

				// Process patches.
				for (PatchedFile eachPatchedFile : commit.getPatches()) {

					RawSkillData commitPatchData = new RawSkillData();
					commitPatchData.setAuthor(login);
					commitPatchData.setType(SkillDataType.COMMIT_PATCH);
					commitPatchData.setTimestamp(commitTime);
					commitPatchData.setContents(eachPatchedFile.getPatch());

					commitPatches.add(commitPatchData);
				}
			}
			authorData.put(SkillDataType.COMMIT_COMMENT, commitMessages);
			authorData.put(SkillDataType.COMMIT_PATCH, commitPatches);
		} else {
			log.debug("No commits were found for [{}]", login);
		}
		log.debug("Commits for [{}] have been queried.", login);

		// get issue comments
		log.debug("Querying issue comments for [{}]...", login);
		List<Comments> authorIssueComments = (List<Comments>) contentDAO.getIssueComments(login);
		if (authorIssueComments.size() != 0) {
			log.debug("About to process [{}] issue comments for [{}]...", authorIssueComments.size(), login);
			Iterator<Comments> it = authorIssueComments.iterator();
			List<RawSkillData> issueComments = new LinkedList<>();
			while (it.hasNext()) {
				Comments comment = it.next();
				RawSkillData authorComment = new RawSkillData();
				authorComment.setAuthor(login);
				authorComment.setType(SkillDataType.ISSUE_COMMENT);
				authorComment.setTimestamp(this.parseTime(comment.getTime()));
				authorComment.setContents(comment.getComment());
				issueComments.add(authorComment);
			}
			authorData.put(SkillDataType.ISSUE_COMMENT, issueComments);
		} else {
			log.debug("No issue comments were found for [{}]", login);
		}
		log.debug("Issue comments for [{}] have been queried.", login);

		// get pull request comments
		log.debug("Querying pull requests for [{}]...", login);
		List<Comments> authorPullRequestComments = (List<Comments>) contentDAO.getPullRequestComments(login);
		if (authorPullRequestComments.size() != 0) {
			log.debug("About to process [{}] pull request comments for [{}]...", authorPullRequestComments.size(), login);
			Iterator<Comments> it = authorPullRequestComments.iterator();
			List<RawSkillData> authorPullRequests = new LinkedList<>();
			while (it.hasNext()) {
				Comments comment = it.next();
				RawSkillData authorPullRequestComment = new RawSkillData();
				authorPullRequestComment.setAuthor(login);
				authorPullRequestComment.setType(SkillDataType.PULL_REQUEST_COMMENT);
				authorPullRequestComment.setTimestamp(this.parseTime(comment.getTime()));
				authorPullRequestComment.setContents(comment.getComment());
				authorPullRequests.add(authorPullRequestComment);
			}
			authorData.put(SkillDataType.PULL_REQUEST_COMMENT, authorPullRequests);
		} else {
			log.debug("No pull requests were found for [{}]", login);
		}
		log.debug("Pull requests for [{}] have been queried.", login);

		// get commit comments
		log.debug("Querying commit comments for [{}]...", login);
		List<Comments> authorCommitComments = (List<Comments>) contentDAO.getCommitComments(login);
		if (authorCommitComments.size() != 0) {
			log.debug("About to process [{}] commit comments for [{}]...", authorCommitComments.size(), login);
			Iterator<Comments> it = authorCommitComments.iterator();
			List<RawSkillData> commitComments = new LinkedList<>();
			while (it.hasNext()) {
				Comments comment = it.next();
				RawSkillData authorCommitComment = new RawSkillData();
				authorCommitComment.setAuthor(login);
				authorCommitComment.setType(SkillDataType.COMMIT_COMMENT);
				authorCommitComment.setTimestamp(this.parseTime(comment.getTime()));
				authorCommitComment.setContents(comment.getComment());
				commitComments.add(authorCommitComment);
			}
			authorData.put(SkillDataType.COMMIT_COMMENT, commitComments);
		} else {
			log.debug("No commit comments were found for [{}]", login);
		}

		log.debug("Commit comments for [{}] have been queried.", login);

		log.info("DB query for [{}] finished.", login);
		return authorData;

	}

	private long parseTime(String time) {
		cachedCalendar.set(Integer.parseInt(time.substring(0, 4)), Integer.parseInt(time.substring(5, 7)) - 1, Integer.parseInt(time.substring(8, 10)));
		cachedCalendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time.substring(11, 13)));
		cachedCalendar.set(Calendar.MINUTE, Integer.parseInt(time.substring(14, 16)));
		cachedCalendar.set(Calendar.SECOND, Integer.parseInt(time.substring(17, 19)));
		return cachedCalendar.getTimeInMillis();
	}
}