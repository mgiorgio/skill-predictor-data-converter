package edu.uci.ics.skillpredictor.dataconverter.matlab;

<<<<<<< HEAD:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabCompressDataFormatExporter.java
import java.io.BufferedWriter;
import java.io.FileWriter;
=======
import java.io.File;
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabDataFormatExporter.java
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class MatlabCompressDataFormatExporter {

	private Map<String, Long> vocabulary;

	private Map<String, Integer> authors;

<<<<<<< HEAD:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabCompressDataFormatExporter.java
	private BufferedWriter writer;

=======
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabDataFormatExporter.java
	private int nextAuthorIndex = 1;

	private long nextWordIndex = 1L;

<<<<<<< HEAD:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabCompressDataFormatExporter.java
	public static final String BAG_OF_WORDS_FILEPATH = MatlabDataConverter.MATLAB_OUTPUT_FOLDER + "/bagofwords.txt";

	public MatlabCompressDataFormatExporter() {
=======
	public MatlabDataFormatExporter() {
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabDataFormatExporter.java
		vocabulary = new LinkedHashMap<>();
		authors = new LinkedHashMap<>();
	}

	public void init() throws IOException {
<<<<<<< HEAD:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabCompressDataFormatExporter.java
		writer = new BufferedWriter(new FileWriter(BAG_OF_WORDS_FILEPATH));
=======
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabDataFormatExporter.java
	}

	public void close() throws IOException {
	}

	public Map<String, Long> getVocabulary() {
		return Collections.unmodifiableMap(vocabulary);
	}

	public Map<String, Integer> getAuthorsTable() {
		return Collections.unmodifiableMap(this.authors);
	}

	public void export(Path path, String author) throws IOException {
		Integer authorIndex = this.getAuthorIndex(author);
		List<String> words = Files.readAllLines(path, Charset.defaultCharset());

		String ds = StringUtils.repeat(String.valueOf(authorIndex), "\n", words.size());
		ds += "\n";

<<<<<<< HEAD:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabCompressDataFormatExporter.java
		for (Entry<String, Integer> frequency : frequencies.entrySet()) {
			Long wordIndex = getIndex(frequency.getKey());
			formatBagOfWordsLine(writer, authorIndex, frequency.getValue(), wordIndex);
			writer.write('\n');
		}
=======
		Files.write(new File(MatlabDataConverter.DS_BAG_OF_WORDS_FILEPATH).toPath(), ds.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);

		String[] wordIndices = new String[words.size()];
		int i = 0;
		for (String word : words) {
			wordIndices[i++] = String.valueOf(getIndex(word));
		}
		String ws = StringUtils.join(wordIndices, "\n");
		ws += "\n";
		Files.write(new File(MatlabDataConverter.WS_BAG_OF_WORDS_FILEPATH).toPath(), ws.getBytes(), StandardOpenOption.APPEND, StandardOpenOption.CREATE);
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabDataFormatExporter.java
	}

	protected Integer getAuthorIndex(String author) {
		Integer index = this.authors.get(author);
		if (index == null) {
			index = nextAuthorIndex++;
			this.authors.put(author, index);
		}
		return index;
	}

<<<<<<< HEAD:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabCompressDataFormatExporter.java
	private void formatBagOfWordsLine(Writer writer, int authorIndex, Integer frequency, Long wordIndex) throws IOException {
		writer.write(String.valueOf(authorIndex));
		writer.write(' ');
		writer.write(wordIndex.toString());
		writer.write(' ');
		writer.write(frequency.toString());
	}

	protected Long getIndex(String word) {
		Long index = this.vocabulary.get(word);
		if (index == null) {
			index = nextWordIndex++;
			this.vocabulary.put(word, index);
=======
	private Long getIndex(String word) {
		if (!this.vocabulary.containsKey(word)) {
			this.vocabulary.put(word, nextWordIndex++);
>>>>>>> dcdd8f132fd2f84a6b85cacacfcdcefc4c1fd660:src/main/java/edu/uci/ics/skillpredictor/dataconverter/matlab/MatlabDataFormatExporter.java
		}
		return index;
	}
}